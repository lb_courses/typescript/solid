import TabelaDePreco from './TabelaDePreco';

export default class TabelaDePrecoPadrao implements TabelaDePreco{
     descontoPara(valor:number) {
        if(valor>5000) return 0.03;
        if(valor>1000) return 0.05;
        return 0;
    }
}
