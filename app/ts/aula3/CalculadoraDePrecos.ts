import TabelaDePreco from './TabelaDePreco';
import ServicoDeEntrega from './ServicoDeEntrega';
import Compra from './Compra';

 export default class CalculadoraDePrecos {
	private  tabela:TabelaDePreco;
  private  entrega:ServicoDeEntrega;
  
	 constructor ( tabela:TabelaDePreco,  entrega:ServicoDeEntrega) {
		this.tabela = tabela;
		this.entrega = entrega;
		
	}
	
      calcula( produto:Compra) {
        const desconto = this.tabela.descontoPara(produto.getValor());
         const frete = this.entrega.para(produto.getCidade());

        return produto.getValor() * (1-desconto) + frete;
    }
}
