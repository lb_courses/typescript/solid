import DezOuVintePorCento from './DezOuVintePorCento';
import QuinzeOuVinteECintoPorCento from './QuinzeOuVinteECintoPorCento';
import IRegraDeCalculo from './IRegraDeCalculo';

interface EnumIdentity { }
export default class Cargo implements EnumIdentity {

    static readonly Desenvolvedor = new DezOuVintePorCento();
    static readonly DBA = new QuinzeOuVinteECintoPorCento();
    static readonly Tester = new QuinzeOuVinteECintoPorCento();
    
    private regra: IRegraDeCalculo;

    private constructor(regra: IRegraDeCalculo) {
      regra = regra;
    }

    public getRegra(){
        return this.regra;
    }

}