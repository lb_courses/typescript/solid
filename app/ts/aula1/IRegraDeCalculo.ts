import Funcionario from './Funcionario';

export default interface IRegraDeCalculo {
  calcula(funcionario: Funcionario):any;
}