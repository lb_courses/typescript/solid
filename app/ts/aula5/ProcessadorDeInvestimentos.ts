import ContaDeEstudante from './ContaDeEstudante';
import ContaComum from './ContaComum';
export default class ProcessadorDeInvestimentos {
  

    contasDoBanco() {
        return [this.umaContaCom(100), this.umaContaCom(150), this.contaDeEstudanteCom(200)];
    }

     contaDeEstudanteCom( amount:number) {
        let  c:ContaDeEstudante = new ContaDeEstudante();
        c.deposita(amount);
        return c;
    }

     umaContaCom( valor:number) {
        let c:ContaComum = new ContaComum();
        c.deposita(valor);
        return c;
    }


    executar() {
      let constas:any = this.contasDoBanco();

      constas.forEach( conta => {
          conta.rende()
          console.log("Novo Saldo:");
          console.log(conta.getSaldo());
      });

    }


}
