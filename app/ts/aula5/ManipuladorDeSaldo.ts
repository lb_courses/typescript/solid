export default class ManipuladorDeSaldo {
	private  saldo: number;

	 deposita( valor:number) {
		this.saldo += valor;
	}

	 saca( valor:number) {
		if (valor <= this.saldo) {
			this.saldo -= valor;
		} else {
			throw new Error("sacar");
		}
	}

	public  getSaldo() {
		return this.saldo;
	}

	 rende( taxa:number) {
		this.saldo *= taxa;
	}
}
