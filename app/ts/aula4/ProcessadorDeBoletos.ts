import Fatura  from './Fatura';
import Pagamento  from './Pagamento';
import Boleto  from './Boleto';
import {MeioDePagamento}  from './MeioDePagamento';

export default class ProcessadorDeBoletos {

	processa(boletos:Boleto[], fatura:Fatura) {

    boletos.forEach( boleto => {
      let pagamento:Pagamento = new Pagamento(boleto.getValor(),
					MeioDePagamento.BOLETO);
			fatura.adicionaPagamento(pagamento);
    });
	
	}
}