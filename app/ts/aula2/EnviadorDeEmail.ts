import NotaFiscal from './NotaFiscal'
import AcaoAposGerarNota from './AcaoAposGerarNota';

export default class EnviadorDeEmail implements AcaoAposGerarNota{
	
	// @Override
    public executa( nf: NotaFiscal) {
        console.log("envia email da nf " + nf.getId())
    }
}