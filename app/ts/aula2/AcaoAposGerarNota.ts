import NotaFiscal from './NotaFiscal';

export default interface AcaoAposGerarNota {
	executa( nf: NotaFiscal):any;
}
