

// sobrecarga construtor
interface INotaFiscal {    
  id : number;
  valorBruto : number;
  imposto : number;
}
export default class NotaFiscal {

    private  id: number;
    private  valorBruto: number;
    private  impostos: number;

    

    //  NotaFiscal( id,  valorBruto,  impostos) {
    //     this.id = id;
    //     this.valorBruto = valorBruto;
    //     this.impostos = impostos;
    // }

    //  constructor( valorBruto,  impostos) {
    //     // this(0, valorBruto, impostos);
    //     this.valorBruto = valorBruto;
    //     this.impostos = impostos;
    // }

    // sobrecarda de contrutores
    constructor();
    constructor(obj?: any) {    
        this.id = obj && obj.x || 0
        this.valorBruto = obj && obj.y || 0
        this.impostos = obj && obj.height || 0
    }   

    getId() {
        return this.id;
    }

     setId( id) {
        this.id = id;
    }
      getValorBruto() {
        return this.valorBruto;
    }
      setValorBruto( valorBruto) {
        this.valorBruto = valorBruto;
    }
      getImpostos() {
        return this.impostos;
    }
      setImpostos( impostos) {
        this.impostos = impostos;
    }

      getValorLiquido() {
        return this.valorBruto - this.impostos;
    }

}